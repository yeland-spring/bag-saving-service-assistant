package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class StupidAssistant extends Assistant {
    private List<Cabinet> cabinets;

    public StupidAssistant(List<Cabinet> cabinets) {
        super(cabinets);
        this.cabinets = cabinets;
    }

    @Override
    public Ticket save(Bag bag) {
        int sizeNumber = bag.getBagSize().getSizeNumber();
        LockerSize lockerSize = selectLockerSize(bag, sizeNumber);
        Ticket ticket;
        ticket = getTicket(bag, lockerSize, 0);
        return ticket;
    }

    private Ticket getTicket(Bag bag, LockerSize lockerSize, int index) {
        Ticket ticket;
        if (index < cabinets.size() - 1) {
            try {
                ticket = cabinets.get(index).save(bag, lockerSize);
            } catch (Exception e) {
                ticket = getTicket(bag, lockerSize, index + 1);
            }
        } else {
            ticket = cabinets.get(index).save(bag, lockerSize);
        }
        return ticket;
    }

    private LockerSize selectLockerSize(Bag bag, int sizeNumber) {
        LockerSize lockerSize;
        if(sizeNumber == 10) {
            lockerSize = LockerSize.SMALL;
        } else if (sizeNumber == 50){
            throw new IllegalArgumentException(String.format("Cannot save %s bag.", bag.getBagSize()));
        } else {
            lockerSize = LockerSize.valueOf(bag.getBagSize().toString());
        }
        return lockerSize;
    }
}
