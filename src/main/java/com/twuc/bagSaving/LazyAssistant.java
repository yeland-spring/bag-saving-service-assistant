package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.List;

public class LazyAssistant extends Assistant {
    private List<Cabinet> cabinets;

    public LazyAssistant(List<Cabinet> cabinets) {
        super(cabinets);
        this.cabinets = cabinets;
    }

    @Override
    public Ticket save(Bag bag) {
        int sizeNumber = bag.getBagSize().getSizeNumber();
        List<LockerSize> lockerSizes = selectLockerSize(bag, sizeNumber);
        Ticket ticket;
        ticket = getTicket(bag, lockerSizes, 0);
        return ticket;
    }

    private Ticket getTicket(Bag bag, List<LockerSize> lockerSizes, int index) {
        Ticket ticket;
        if (index < cabinets.size() - 1) {
            try {
                ticket = selectLockerToSave(bag, lockerSizes, cabinets.get(index));
            } catch (Exception e) {
                ticket = getTicket(bag, lockerSizes, index + 1);
            }
        } else {
            ticket = selectLockerToSave(bag, lockerSizes, cabinets.get(index));
        }
        return ticket;
    }

    private Ticket selectLockerToSave(Bag bag, List<LockerSize> lockerSizes, Cabinet cabinet) {
        return saveToLocker(bag, lockerSizes, cabinet, 0);
    }

    private Ticket saveToLocker(Bag bag, List<LockerSize> lockerSizes, Cabinet cabinet, int index) {
        Ticket ticket;
        if(index < lockerSizes.size() -1) {
            try {
                ticket = cabinet.save(bag, lockerSizes.get(index));
            } catch (Exception e) {
                ticket = saveToLocker(bag, lockerSizes, cabinet, index + 1);
            }
        } else {
            ticket = cabinet.save(bag, lockerSizes.get(index));
        }
        return ticket;
    }

    private List<LockerSize> selectLockerSize(Bag bag, int sizeNumber) {
        List<LockerSize> lockerSizes;
        switch (sizeNumber) {
            case 10:
            case 20: {
                lockerSizes = Arrays.asList(LockerSize.SMALL, LockerSize.MEDIUM, LockerSize.BIG);
                break;
            }
            case 30: {
                lockerSizes = Arrays.asList(LockerSize.MEDIUM, LockerSize.BIG);
                break;
            }
            case 40: {
                lockerSizes = Arrays.asList(LockerSize.BIG);
                break;
            }
            default: {
                throw new IllegalArgumentException(String.format("Cannot save %s bag.", bag.getBagSize()));
            }
        }
        return lockerSizes;
    }
}
