package com.twuc.bagSaving;

import java.util.List;

public abstract class Assistant {
    private List<Cabinet> cabinets;

    public Assistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public abstract Ticket save(Bag bag);

    public Bag get(Ticket ticket) {
        int index = 0;
        return getBag(ticket, index);
    }

    private Bag getBag(Ticket ticket, int index) {
        Bag bag;
        if(index < cabinets.size() - 1) {
            try {
                bag = cabinets.get(index).getBag(ticket);
            } catch (Exception e) {
                bag = getBag(ticket, index + 1);
            }
        } else {
            bag = cabinets.get(index).getBag(ticket);
        }
        return bag;
    }
}
