package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagByLazyAssistantTest {

    @Test
    void should_save_bag_when_give_a_big_bag_to_lazy_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.BIG);
        LazyAssistant lazyAssistant = new LazyAssistant(Arrays.asList(cabinet));
        Ticket ticket = lazyAssistant.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_return_bag_when_give_ticket_to_lazy_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.BIG);
        LazyAssistant lazyAssistant = new LazyAssistant(Arrays.asList(cabinet));
        Ticket ticket = lazyAssistant.save(bag);
        Bag bagGet = lazyAssistant.get(ticket);
        assertSame(bag, bagGet);
    }

    @Test
    void should_save_to_bigger_locker_and_get() {
        LockerSetting mediumLocker = new LockerSetting(LockerSize.MEDIUM, 0);
        LockerSetting bigLocker = new LockerSetting(LockerSize.BIG, 10);
        Cabinet cabinet = new Cabinet(mediumLocker, bigLocker);

        LazyAssistant lazyAssistant = new LazyAssistant(Arrays.asList(cabinet));

        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = lazyAssistant.save(bag);
        assertNotNull(ticket);
        Bag bagGet = lazyAssistant.get(ticket);
        assertSame(bag, bagGet);
    }

    @Test
    void should_save_to_another_cabinet_when_cabinet_is_full() {
        LockerSetting mediumLocker = new LockerSetting(LockerSize.MEDIUM, 0);
        LockerSetting bigLocker = new LockerSetting(LockerSize.BIG, 10);
        Cabinet cabinet = new Cabinet(mediumLocker, bigLocker);

        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        LazyAssistant lazyAssistant = new LazyAssistant(Arrays.asList(cabinet, anotherCabinet));

        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = lazyAssistant.save(bag);
        assertNotNull(ticket);
    }
}
