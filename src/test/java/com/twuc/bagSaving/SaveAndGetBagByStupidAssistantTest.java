package com.twuc.bagSaving;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithFullLockers;
import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SaveAndGetBagByStupidAssistantTest {
    @Test
    void should_save_bag_when_give_a_bag_to_stupid_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.BIG);
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));
        Ticket ticket = stupidAssistant.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_return_bag_when_give_ticket_to_stupid_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.BIG);
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));
        Ticket ticket = stupidAssistant.save(bag);
        Bag bagGet = stupidAssistant.get(ticket);
        assertSame(bag, bagGet);
    }

    @Test
    void should_throw_if_correspond_lockers_are_full() {
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1);
        Bag bag = new Bag(BagSize.BIG);
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> stupidAssistant.save(bag));
        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_throw_if_lockers_not_support() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.HUGE);
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));
        IllegalArgumentException error = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.save(bag));
        assertEquals("Cannot save HUGE bag.", error.getMessage());
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));

        final IllegalArgumentException error = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.get(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));
        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.get(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_used() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet));

        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(bag);
        stupidAssistant.get(ticket);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.get(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @Test
    void should_save_bag_given_two_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet, anotherCabinet));
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(bag);
        assertNotNull(ticket);
        assertSame(cabinet.getBag(ticket), bag);
    }

    @Test
    void should_save_bag_to_second_when_first_cabinet_full() {
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1);
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet, anotherCabinet));
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(bag);
        assertNotNull(ticket);
        assertSame(anotherCabinet.getBag(ticket), bag);
    }

    @Test
    void should_get_bag_when_first_cabinet_full() {
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1);
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();

        List<Cabinet> cabinets = Arrays.asList(cabinet, anotherCabinet);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);

        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(bag);
        Bag bagGet = stupidAssistant.get(ticket);
        assertSame(bag, bagGet);
    }

    @Test
    void should_get_bag_by_another_stupid_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();

        StupidAssistant stupidAssistant = new StupidAssistant(Arrays.asList(cabinet, anotherCabinet));
        StupidAssistant anotherAssistant = new StupidAssistant(Arrays.asList(cabinet, anotherCabinet));

        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(bag);

        Bag bagGet = anotherAssistant.get(ticket);
        assertSame(bag, bagGet);
    }


}
